package com.example.practice3;

import android.annotation.SuppressLint;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    Button btn;

    Spinner drop1, drop2;
    EditText text2;
    TextView text3;
    String fromString, toStrings;
    double valuefrom, valueto;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btn = findViewById(R.id.btn);
        drop1 = findViewById(R.id.drop1);
        drop2 = findViewById(R.id.drop2);
        text2 = findViewById(R.id.text2);
        text3 = findViewById(R.id.text3);
        text2.setText("1");

        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fromString = drop1.getSelectedItem().toString();
                valuefrom =Double.parseDouble(text2.getText().toString());
                toStrings = drop2.getSelectedItem().toString();

                text3.setText(currencyConvert(valuefrom,0,fromString,toStrings) +"" );
                Toast.makeText(MainActivity.this,valuefrom + " " + text3.getText(), Toast.LENGTH_SHORT).show();
            }
        });


        String[] currency1 = {"USD","EUR","GBP","JPY","NGN"};

        ArrayAdapter cc = new ArrayAdapter<String>(this, androidx.appcompat.R.layout.support_simple_spinner_dropdown_item,currency1);
        drop1.setAdapter(cc);
        drop1.setSelection(cc.getPosition("USD"));
        drop1.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {


            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        String[] currency2 = {"NGN","JPY","GBP","EUR","USD"};
        ArrayAdapter cc2 = new ArrayAdapter<String>(this, androidx.appcompat.R.layout.support_simple_spinner_dropdown_item,currency2);
        drop2.setAdapter(cc2);
        drop2.setSelection(cc2.getPosition("NGN"));
        drop2.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

    }
    private double currencyConvert(double valuefrom, double convert, String fromString, String toString)
    {
        return valuefrom*defaultRate(fromString,toString);
    }

    private double defaultRate(String fromString, String toString)
    {
        double defaultValue =0;

        switch (fromString)
        {
            case "USD":
                switch (toString)
                {
                    case "USD":
                        defaultValue = 1;
                        break;
                    case "NGN":
                        defaultValue = 800;
                        break;
                    case "JPY":
                        defaultValue = 141.80;
                        break;
                    case "GBP":
                        defaultValue = 0.78;
                        break;
                    case "EUR":
                        defaultValue= 0.90;
                        break;
                    default: break;
                }
                break;
            case "NGN":
                switch (toString)
                {
                    case "NGN":
                        defaultValue = 1;
                        break;
                    case "USD":
                       defaultValue = 0.0013;
                       break;
                    case "JPY":
                       defaultValue = 0.18;
                       break;
                    case "GBP":
                        defaultValue = 0.00098;
                        break;
                    case "EUR":
                        defaultValue = 0.0011;
                    default: break;
                }
                break;
            case "JPY":
                switch (toString)
                {
                case "JPY":
                    defaultValue = 1;
                    break;
                case "NGN":
                    defaultValue = 5.48;
                    break;
                case "USD":
                    defaultValue = 0.0071;
                    break;
                case "GBP":
                    defaultValue = 0.0055;
                    break;
                case "EUR":
                    defaultValue = 0.0063;
                    break;
                }
                break;
            case "GBP":
                switch (toString)
                {
                    case "GBP":
                        defaultValue = 1;
                        break;
                    case "NGN":
                        defaultValue = 998.38;
                        break;
                    case "USD":
                        defaultValue = 1.29;
                        break;
                    case "EUR":
                        defaultValue = 1.15;
                        break;
                    case "JPY":
                        defaultValue = 490.76;
                        break;
                    default: break;
                }
                break;
            case "EUR":
                switch (toString)
                {
                    case "EUR":
                        defaultValue = 1;
                        break;
                    case "NGN":
                        defaultValue = 863.86;
                        break;
                    case "USD":
                        defaultValue = 1.11;
                        break;
                    case "JPY":
                        defaultValue = 157.87;
                        break;
                    case "GBP":
                        defaultValue = 0.87;
                        break;
                    default: break;
                }
            default: break;
        }
        return defaultValue;
    }
}